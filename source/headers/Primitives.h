#pragma once

#include <vector>
#include <cstdint>

struct Mesh {
	std::vector<uint32_t> indicies;
	std::vector<float> positions;
};

struct MeshColor {
	std::vector<uint32_t> indicies;
	std::vector<float> positions;
	std::vector<float> colors;
};


class Primitives {
public:
	static Mesh Cube(float width, float height, float depth);
	static MeshColor Cube(float width, float height, float depth, float *data, unsigned int size = 24);
};