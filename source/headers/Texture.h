#pragma once

#include "Renderer.h"
#include <string>

class Texture
{
public:
	Texture(const std::string &path);
	~Texture();

	inline int GetWidth() { return m_Width; }
	inline int GetHeight() { return m_Height; }
	inline int GetBPP() { return m_BPP; }

	void Bind(unsigned int slot = 0) const;
	void Unbind() const;

private:
	unsigned int m_RendererID;
	std::string m_FilePath;
	unsigned char* m_LocalBuffer;
	int m_Width, m_Height, m_BPP;
};

