#pragma once

#include <GL/glew.h>
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"

#ifdef ALX_DEBUG
#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x)	GLClearError();\
						x;\
						ASSERT(GLLogCall(#x, __FILE__, __LINE__))
#else
#define GLCall(x) x
#define ASSERT(x) x
#endif

void GLClearError();

bool GLLogCall(const char* function, const char* file, int line);

//supposedly returns size of array? not sure if it works properly.
template <typename T, unsigned int N> inline static unsigned int SizeOfArray(const T(&)[N]) {
	return N;
}


class Renderer {
public:
	void Clear() const;
	void Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;
	Renderer();
	~Renderer();
};

