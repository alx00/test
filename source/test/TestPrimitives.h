#pragma once
#include "Test.h"

namespace test {
	class TestPrimitives : public Test
	{
	public:
		TestPrimitives();
		~TestPrimitives();
		void OnUpdate(float DeltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	};
}


