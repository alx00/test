#include "TestMultipleObjects.h"
#include "imgui.h"

namespace test {

	TestMultipleObjects::TestMultipleObjects():
		quad{
		-0.5f, -0.5f, 0.0f, 0.0f,	//0
		0.5f, -0.5f, 1.0f, 0.0f,	//1
		0.5f,  0.5f, 1.0f, 1.0f,	//2
		-0.5f,  0.5f, 0.0f, 1.0f	//3
		},
		indices{
		0, 1, 2, 2, 0, 3
		},
		vb(quad, 16*sizeof(float)), 
		shader(path), 
		ib(indices, 6), 
		m_ClearColor{ 1.0f, 1.0f, 1.0f, 1.0f },
		m_Color{1.0f, 1.0f, 1.0f, 1.0f},
		proj(glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f)),
		view(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f))),
		model(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f))),
		mvp(proj * view * model)
	{

		vb.Bind();
		ib.Bind();
		shader.Bind();
		va.Bind();

		layout.Push<GLfloat>(2);
		layout.Push<GLfloat>(2);
		va.AddBuffer(vb, layout);

		shader.SetUniform4f("u_Color", 0.5f, 0.5f, 0.5f, 1.0f);
		shader.SetUniform1i("u_Texture", 0);
		shader.SetUniformMat4f("u_MVP", mvp);
		GLCall(glDisable(GL_DEPTH_TEST));
	}

	TestMultipleObjects::~TestMultipleObjects(){
		for (size_t i = 0; i < textures.size(); i++) {
			textures.erase(textures.begin() + i);
		}
		shader.Unbind();
		va.Unbind();
		ib.Unbind();
	}

	void TestMultipleObjects::OnUpdate(float DeltaTime){
		/*
		this is just to change the textures, by changing which texture object is being pointed at
		when we later draw we're drawing with a DIFFERENT texture object :>
		*/
		if (swap) {
			Texture* temp = textures[0];
			textures[0] = textures[1];
			textures[1] = textures[2];
			textures[2] = temp;
		}
	}

	void TestMultipleObjects::DrawObject(unsigned int TexIndex, float* transMatrix) {
		textures[TexIndex]->Bind();
		model = glm::translate(glm::mat4(1.0f), glm::vec3(transMatrix[0], transMatrix[1], transMatrix[2]));
		mvp = proj * view * model;
		shader.SetUniformMat4f("u_MVP", mvp);
		GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));
		textures[TexIndex]->Unbind();
	}

	void TestMultipleObjects::OnRender(){
		GLCall(glEnable(GL_BLEND));
		GLCall(glClearColor(m_ClearColor[0], m_ClearColor[1], m_ClearColor[2], m_ClearColor[3]));
		shader.Bind();
		va.Bind();
		ib.Bind();
		shader.SetUniform1i("u_Texture", 0);
		shader.SetUniform4f("u_Color", m_Color[0], m_Color[1], m_Color[2], m_Color[3]);
		GLCall(glClear(GL_COLOR_BUFFER_BIT));
		for (unsigned int i = 0; i < textures.size(); i++) {
			DrawObject(i, m_Pos + (i*3));
		}
	}

	void TestMultipleObjects::OnImGuiRender(){
		ImGui::Checkbox("Texture swapping", &swap);
		for (size_t i = 0; i < textures.size(); i++) {
			const char label[7] = {'I', 't', 'e', 'm', ' ',static_cast<const char>(i + 48), '\0'};
			ImGui::DragFloat2(label, m_Pos + (i * 3), 0.1f, -2.0f, 2.0f);
		}
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::ColorEdit4("Clear Color", m_ClearColor);
		//ImGui::ColorEdit4("Box Color", m_Color);
	}



}
