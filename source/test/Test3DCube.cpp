#include "Test3DCube.h"
#include "imgui.h"

#include <iostream>

namespace test {
	/*
	
	-width, -height, -depth, // 0
	width, -height, -depth, // 1
	width,  height, -depth, // 2
	-width,  height, -depth, // 3
	-width, -height,  depth, // 4
	width, -height,  depth, // 5
	width,  height,  depth, // 6
	-width,  height,  depth // 7
	
	*/
	Test3DCube::Test3DCube() :
		cube(Primitives::Cube(0.2f, 0.2f, 0.2f)),
		vb(cube.positions.data(), static_cast<unsigned int>(cube.positions.size() * sizeof(float))),
		shader(path),
		ib(cube.indicies.data(), static_cast<unsigned int>(cube.indicies.size())),
		m_ClearColor{ 1.0f, 1.0f, 1.0f, 1.0f },
		m_Color{ 0.5f, 0.5f, 0.5f, 1.0f },
		m_Cherno{0.0f, 0.0f, -3.0f},
		//proj(glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, 0.1f, 10.0f)),
		proj(glm::perspective(glm::radians(45.0f), 1.f, 0.1f, 100.f)),
		view(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f))),
		model(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f))),
		mvp(proj * view * model),
		rotation{}
	{
		vb.Bind();
		ib.Bind();
		shader.Bind();
		va.Bind();

		layout.Push<GLfloat>(3);

		va.AddBuffer(vb, layout);

		shader.SetUniform4f("u_Color", 0.5f, 0.5f, 0.5f, 1.0f);
		shader.SetUniformMat4f("u_MVP", mvp);
	}

	Test3DCube::~Test3DCube() {
		shader.Unbind();
		va.Unbind();
		ib.Unbind();
	}

	void Test3DCube::OnUpdate(float DeltaTime) {

	}

	void Test3DCube::OnRender() {
		GLCall(glEnable(GL_DEPTH_TEST));
		shader.Bind();
		va.Bind();
		ib.Bind();
		shader.SetUniform4f("u_Color", m_Color[0], m_Color[1], m_Color[2], m_Color[3]);
		model = glm::translate(glm::mat4(1.0f), glm::vec3(m_Cherno[0], m_Cherno[1], m_Cherno[2]));
		model *= glm::rotate(glm::mat4(1.0f), glm::radians(rotation[0]), glm::vec3(0.0f, 0.0f, 1.0f));
		model *= glm::rotate(glm::mat4(1.0f), glm::radians(rotation[1]), glm::vec3(0.0f, 1.0f, 0.0f));
		model *= glm::rotate(glm::mat4(1.0f), glm::radians(rotation[2]), glm::vec3(1.0f, 0.0f, 0.0f));
		/*view = glm::lookAt(
			glm::vec3(1.2f, 1.2f, 1.2f),
			glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 0.0f, 1.0f)
		);*/
		mvp = proj * view * model;
		shader.SetUniformMat4f("u_MVP", mvp);
		GLCall(glClearColor(m_ClearColor[0], m_ClearColor[1], m_ClearColor[2], m_ClearColor[3]));
		GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
		GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));	
	}

	void Test3DCube::OnImGuiRender() {
		ImGui::ColorEdit4("Clear Color", m_ClearColor);
		ImGui::ColorEdit4("Box Color", m_Color);
		ImGui::DragFloat3("Box", m_Cherno, 0.1f, -3.0f, 3.0f);
		ImGui::DragFloat3("Rotation", rotation, 1.0f, 0.0f, 360.f);
		ImGui::Text("Application average %.2f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}

}