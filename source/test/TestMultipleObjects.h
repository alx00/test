#pragma once

#include "Test.h"
#include "Renderer.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "Texture.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <utility>
/*
included in renderer:
	#include "VertexArray.h"
	#include "IndexBuffer.h"
	#include "Shader.h"
*/


namespace test {

	class TestMultipleObjects : public Test{
	public:
		TestMultipleObjects();
		~TestMultipleObjects();
		void OnUpdate(float DeltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	private:

		void DrawObject(unsigned int TexIndex, float* transMatrix);

		GLfloat quad[16];
		GLuint indices[6];
		float m_Color[4];
		float m_ClearColor[4];

		glm::mat4 mvp;
		glm::mat4 model;
		glm::mat4 proj;
		glm::mat4 view;


		//I tried to do std::vector<std::pair<Texture*, float*>> but 1. thats alot of effort to be typing (also had to std::make_pair(Texture(), float*))
		//2. it wouldnt work, I assume because of some copy/move stuff
		// for each texture there has to be 3 positions, otherwise it will be reading invalid memory.
		std::vector<Texture*> textures = {
			new Texture("res\\textures\\droid.png"),
			new Texture("res\\textures\\cherno-logo.jpg"),
			new Texture("res\\textures\\bird.jpg")
		};
		float m_Pos[9] = {
			0.0f, 0.0f, 0.0f,
			1.0f, 1.0f, 1.0f,
			0.5f, 0.5f, 0.5f
		};

		bool swap = true;


		VertexBuffer vb;
		VertexBufferLayout layout;
		VertexArray va;
		IndexBuffer ib;
		const std::string path = "res\\shaders\\BasicTex.glsl";
		Shader shader;
	};
}

