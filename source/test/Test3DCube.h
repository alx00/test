#pragma once

#include "Test.h"
#include "Renderer.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "Texture.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Primitives.h"

#include <vector>

/*
included in renderer:
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"
*/


namespace test {

	class Test3DCube : public Test {
	public:
		Test3DCube();
		~Test3DCube();
		void OnUpdate(float DeltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	private:
		Mesh cube;
		float m_Color[4];
		float m_ClearColor[4];
		float m_Cherno[3];
		float rotation[3];

		glm::mat4 mvp;
		glm::mat4 model;
		glm::mat4 proj;
		glm::mat4 view;

		VertexBuffer vb;
		VertexBufferLayout layout;
		VertexArray va;
		IndexBuffer ib;
		const std::string path = "res\\shaders\\Basic.glsl";
		Shader shader;
	};
}