#include "Primitives.h"


Mesh Primitives::Cube(float width, float height, float depth) {
	Mesh temp;
	temp.positions = {
		-width, -height, -depth, // 0
		width, -height, -depth, // 1
		width,  height, -depth, // 2
		-width,  height, -depth, // 3
		-width, -height,  depth, // 4
		width, -height,  depth, // 5
		width,  height,  depth, // 6
		-width,  height,  depth // 7
	};

	temp.indicies = {
		0, 2, 1, 0, 3, 2,
		1, 2, 6, 6, 5, 1,
		4, 5, 6, 6, 7, 4,
		2, 3, 6, 6, 3, 7,
		0, 7, 3, 0, 4, 7,
		0, 1, 5, 0, 5, 4
	};

	return temp;
}

MeshColor Primitives::Cube(float width, float height, float depth, float *data, unsigned int size) {
	MeshColor temp;
	temp.positions = {
		-width, -height, -depth, // 0
		width, -height, -depth, // 1
		width,  height, -depth, // 2
		-width,  height, -depth, // 3
		-width, -height,  depth, // 4
		width, -height,  depth, // 5
		width,  height,  depth, // 6
		-width,  height,  depth // 7
	};

	temp.indicies = {
		0, 2, 1, 0, 3, 2,
		1, 2, 6, 6, 5, 1,
		4, 5, 6, 6, 7, 4,
		2, 3, 6, 6, 3, 7,
		0, 7, 3, 0, 4, 7,
		0, 1, 5, 0, 5, 4
	};
	//if supplied all 24 colors, put them all in the buffer
	if (size == 24) {
		for (unsigned int i = 0; i < size; i++) {
			temp.colors.emplace_back(data[i]);
		}
	}
	//otherwise put the supplied colors in buffer, and fill rest with 1.0f (the "highest color" before wrap)
	else {
		for (unsigned int i = 0; i < size; i++) {
			temp.colors.emplace_back(data[i]);
		}
		int diff = 24 - size;
		for (int i = 0; i < diff; i++) {
			temp.colors.emplace_back(1.0f);
		}
	}


	return temp;
}