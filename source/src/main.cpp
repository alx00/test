
#include <iostream>

#include <GL/glew.h>
#include <glfw/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include "Renderer.h"
#include "Primitives.h"


#include "TestClearColor.h"
#include "TestMultipleObjects.h"
#include "Test3DCube.h"
#include "TestPrimitives.h"


#include <fstream>

void GLAPIENTRY
MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	std::string info[5]; //indexed as param, except length

	switch (source) {
	case GL_DEBUG_SOURCE_API:
		info[0] = "GL_DEBUG_SOURCE_API";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		info[0] = "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		info[0] = "GL_DEBUG_SOURCE_SHADER_COMPILER";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		info[0] = "GL_DEBUG_SOURCE_THIRD_PARTY";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		info[0] = "GL_DEBUG_SOURCE_APPLICATION";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		info[0] = "GL_DEBUG_SOURCE_OTHER";
		break;
	default:
		info[0] = "unknown";
		break;
	}

	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		info[1] = "GL_DEBUG_TYPE_ERROR";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		info[1] = "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		info[1] = "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		info[1] = "GL_DEBUG_TYPE_PORTABILITY";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		info[1] = "GL_DEBUG_TYPE_PERFORMANCE";
		break;
	case GL_DEBUG_TYPE_MARKER:
		info[1] = "GL_DEBUG_TYPE_MARKER";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		info[1] = "GL_DEBUG_TYPE_PUSH_GROUP";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		info[1] = "GL_DEBUG_TYPE_POP_GROUP";
		break;
	case GL_DEBUG_TYPE_OTHER:
		info[1] = "GL_DEBUG_TYPE_OTHER";
		break;
	default:
		info[1] = "unknown";
		break;
	}


	switch (severity) {
	case GL_DEBUG_SEVERITY_HIGH:
		info[3] = "GL_DEBUG_SEVERITY_HIGH";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		info[3] = "GL_DEBUG_SEVERITY_MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		info[3] = "GL_DEBUG_SEVERITY_LOW";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		info[3] = "GL_DEBUG_SEVERITY_NOTIFICATION";
		break;
	default:
		info[3] = "unknown";
		break;
	}

	info[2] = id;
	info[4] = message;

	std::ofstream *temp = (std::ofstream*)userParam;
	if (severity != GL_DEBUG_SEVERITY_NOTIFICATION){
		*temp << "Severity: " << info[3] << "\nSource: " << info[0] << "\nType: " << info[1] << "\nID: " << info[2] << "\nMessage: " << info[4] << "\n\n";
		std::cout << "Severity: " << info[3] << "\nSource: " << info[0] << "\nType: " << info[1] << "\nID: " << info[2] << "\nMessage: " << info[4] << "\n\n";
	}	

	//*temp << msg;
	//write to file

}



int main()
{		
	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, 0);


	GLFWwindow* window;
	window = glfwCreateWindow(720, 720, "Hello World", NULL, NULL);
	if (!window){
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	GLenum err = glewInit();
	if (GLEW_OK != err){
		std::cout << "Error: " << glewGetErrorString(err) << "\n";
	}

	std::cout << "Vendor: " << glGetString(GL_VENDOR) << "\n";
	std::cout << "GL Version: " << glGetString(GL_VERSION) << "\n";
	std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
	std::cout << "GL Renderer Name: " << glGetString(GL_RENDERER) << "\n\n";


	//open a file to log to and pass to debug callback.
	std::ofstream *logFile = new std::ofstream;
	logFile->open("log.txt", std::ofstream::app);

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, logFile);

	glfwSwapInterval(0); //0 = uncapped fps, 1 = monitor capped (144, 60 etc)

	GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	GLCall(glDepthFunc(GL_LEQUAL));

	//GLCall(glEnable(GL_DEPTH_TEST)); apparently GL_BLEND and GL_DEPTH_TEST can't be enabled at the same time :?

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); static_cast<void>(io);
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init();
	ImGui::StyleColorsDark();

	//Test scenes
	test::Test* currentTest = nullptr;
	test::TestMenu* testMenu = new test::TestMenu(currentTest);
	currentTest = testMenu;

	testMenu->RegisterTest<test::TestMultipleObjects>("Multiple Objects");
	testMenu->RegisterTest<test::TestClearColor>("Clear Color");
	testMenu->RegisterTest<test::Test3DCube>("3D Cube");
	testMenu->RegisterTest<test::TestPrimitives>("Primitives");

	while (!glfwWindowShouldClose(window))
	{

		//imgui stuff, the imgui rendering happens between NewFrame() and Render()
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		if (currentTest) {
			currentTest->OnUpdate(0.0f);\
			currentTest->OnRender();
			ImGui::Begin("Test");
			if (currentTest != testMenu && ImGui::Button("<-")) {
				delete currentTest;
				currentTest = testMenu;
			} else if(currentTest == testMenu) {
				GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
				GLCall(glClear(GL_COLOR_BUFFER_BIT));
			}

			currentTest->OnImGuiRender();
			ImGui::End();
		}


		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	delete currentTest;
	if (currentTest != testMenu) {
		delete testMenu;
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwTerminate();
	logFile->close();
	delete logFile;
	return 0;
}